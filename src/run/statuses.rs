use crate::run::XbpsStatus;

const N_FIELDS: usize = 9;

#[derive(Debug, Clone)]
pub struct Statuses {
    bad: Vec<String>,
    bad_checksum: Vec<String>,
    bad_fetch: Vec<String>,
    missing_dependency: Vec<String>,
    missing_set_id: Vec<String>,
    unsupported_arch: Vec<String>,
    no_cross: Vec<String>,
    broken: Vec<String>,
    good: Vec<String>,
}

impl Statuses {
    pub fn new() -> Self {
        Self {
            bad: vec![],
            bad_checksum: vec![],
            bad_fetch: vec![],
            missing_dependency: vec![],
            missing_set_id: vec![],
            unsupported_arch: vec![],
            no_cross: vec![],
            broken: vec![],
            good: vec![],
        }
    }

    pub fn add(&mut self, status: XbpsStatus, package: String) {
        let target = match status {
            XbpsStatus::Good => &mut self.good,
            XbpsStatus::Bad => &mut self.bad,
            XbpsStatus::MissingDependency => &mut self.missing_dependency,
            XbpsStatus::MissingSetId => &mut self.missing_set_id,
            XbpsStatus::BadChecksum => &mut self.bad_checksum,
            XbpsStatus::BadFetch => &mut self.bad_fetch,
            XbpsStatus::UnsupportedArch => &mut self.unsupported_arch,
            XbpsStatus::NoCross => &mut self.no_cross,
            XbpsStatus::Broken => &mut self.broken,
        };

        target.push(package);
    }

    pub fn iter(&self) -> <&Self as IntoIterator>::IntoIter {
        self.into_iter()
    }
}

impl<'a> IntoIterator for &'a Statuses {
    type Item = (XbpsStatus, &'a [String]);

    type IntoIter = std::array::IntoIter<Self::Item, N_FIELDS>;

    fn into_iter(self) -> Self::IntoIter {
        let fields: [(_, &[String]); N_FIELDS] = [
            (XbpsStatus::Bad, &self.bad),
            (XbpsStatus::BadChecksum, &self.bad_checksum),
            (XbpsStatus::BadFetch, &self.bad_fetch),
            (XbpsStatus::MissingDependency, &self.missing_dependency),
            (XbpsStatus::MissingSetId, &self.missing_set_id),
            (XbpsStatus::UnsupportedArch, &self.unsupported_arch),
            (XbpsStatus::NoCross, &self.no_cross),
            (XbpsStatus::Broken, &self.broken),
            (XbpsStatus::Good, &self.good),
        ];

        fields.into_iter()
    }
}

impl IntoIterator for Statuses {
    type Item = (XbpsStatus, Vec<String>);

    type IntoIter = std::array::IntoIter<Self::Item, N_FIELDS>;

    fn into_iter(self) -> Self::IntoIter {
        let fields = [
            (XbpsStatus::Bad, self.bad),
            (XbpsStatus::BadChecksum, self.bad_checksum),
            (XbpsStatus::BadFetch, self.bad_fetch),
            (XbpsStatus::MissingDependency, self.missing_dependency),
            (XbpsStatus::MissingSetId, self.missing_set_id),
            (XbpsStatus::UnsupportedArch, self.unsupported_arch),
            (XbpsStatus::NoCross, self.no_cross),
            (XbpsStatus::Broken, self.broken),
            (XbpsStatus::Good, self.good),
        ];

        fields.into_iter()
    }
}
