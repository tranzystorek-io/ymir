use std::path::PathBuf;

use clap::{Args, CommandFactory, Parser, Subcommand};
use clap_complete::Shell;

use crate::todo::TodoSpecs;

pub fn generate_completion(shell: Shell) {
    let cli = &mut Cli::command();
    clap_complete::generate(
        shell,
        cli,
        cli.get_name().to_string(),
        &mut std::io::stdout(),
    );
}

#[derive(Parser)]
#[command(version)]
pub struct Cli {
    /// Path to the local void-packages repository
    #[arg(short = 'D', long, value_name = "PATH", global = true)]
    pub distdir: Option<PathBuf>,

    #[command(subcommand)]
    pub command: Command,
}

#[derive(Subcommand)]
pub enum Command {
    /// Execute bulk package build
    Run(RunArgs),

    /// Report results of last bulk package build
    Report(ReportArgs),

    /// Manage the todo list file that specifies which packages will be built
    #[command(subcommand)]
    Todo(TodoCommand),

    /// Generate shell completion script
    Completion {
        /// Shell to generate completion for
        #[arg(value_enum)]
        shell: Shell,
    },
}

#[derive(Subcommand)]
pub enum TodoCommand {
    /// Populate the todo list file
    Grab(TodoSpecs),

    /// Empty the todo list file
    Clear,

    /// List packages to be built
    List,
}

#[derive(Args)]
pub struct RunArgs {
    /// Number of concurrent build processes (and masterdirs)
    #[arg(short, long, value_name = "N", value_parser = clap::value_parser!(u64).range(1..))]
    pub procs: Option<u64>,

    /// Desired parallelism per build process, by default maximum available parallelism
    #[arg(short, long, value_name = "N", value_parser = clap::value_parser!(u64).range(1..))]
    pub jobs: Option<u64>,

    /// Time period in seconds between build process completion polls
    #[arg(short = 't', long = "poll", value_name = "DURATION", value_parser = clap::value_parser!(u64).range(1..))]
    pub poll_period: Option<u64>,

    /// Arch to bootstrap the masterdirs with
    #[arg(short = 'A', long, value_name = "ARCH")]
    pub masterdir_arch: Option<String>,

    /// Arch to cross-build for
    #[arg(short = 'a', long, value_name = "ARCH")]
    pub cross_arch: Option<String>,

    /// xbps-src flags
    #[arg(value_name = "FLAGS", last = true)]
    pub xbps_src_flags: Vec<String>,
}

#[derive(Args)]
pub struct ReportArgs {
    /// Show only failed packages
    #[arg(short, long)]
    pub failures: bool,
}
